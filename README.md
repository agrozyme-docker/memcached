# Summary

Source: https://gitlab.com/agrozyme-docker/memcached

Free & open source, high-performance, distributed memory object caching system.

# Environment Variables

When you start the image, you can adjust the configuration of the instance by passing one or more environment variables on the docker run command line.

## MEMCACHED_CACHE_SIZE

These variables are optional, tells memcached how much RAM to use for item storage (in megabytes).
The default value is `64`, if you set a number less `64`, it will be `64`.
