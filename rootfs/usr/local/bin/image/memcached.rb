# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Memcached
      module Build
        def self.main
          System.run('apk add --no-cache memcached')
        end
      end

      module Run
        def self.main
          size = ENV.fetch('MEMCACHED_CACHE_SIZE', '64')
          Shell.update_user
          System.execute('memcached', { user: USER, 'memory-limit': size }, '')
        end
      end
    end
  end
end
